import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Pressable, View } from "react-native";
import { TextInputMask } from "react-native-masked-text";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faArrowLeft, faArrowRight } from "@fortawesome/free-solid-svg-icons";
import tw from "../../libs/tailwind";

interface ITimeField {
  class?: string; // tailwind classes
  value: string | null; // Если не задано отображаем --:--
  onChange?: (time: string) => void; // событие должно вызываться при снятии фокуса с компонента.
  useButtons?: boolean; // если true - отображаем кнопки "<" слева и справа ">" + или - 1 час соответственно
}

// utils/unMask.ts
// Функция для извлечения часов и минут из строки времени
const unMask = (time: string): [string, string] => {
  let [hh, mm] = time.split(":");

  if (!Boolean(hh)) hh = "00";
  if (!Boolean(mm)) mm = "00";

  return [hh, mm];
};

type useTimeInputReturn = [
  string,
  (text: string) => void,
  () => void,
  () => void
];

// Данный хук нужно вынести в отдельный файл,
// но для удобства проверки оставил здесь
// hooks/useTimeInput.ts
const useTimeInput = (initTime: string | null): useTimeInputReturn => {
  const [time, setTime] = useState("");

  // Обработчик ввода времени
  const handleTimeChange = useCallback((formatted: string) => {
    if (formatted.length > 5) return;

    const mm = formatted.split(":")[1];

    // Если минуты начинаются с числа больше 5, заменяем первую цифру на 5
    if (Boolean(mm) && Number(mm[0]) > 5) {
      const result = formatted.substring(0, 3) + "5" + formatted.substring(4);
      setTime(result);
    } else {
      setTime(formatted);
    }
  }, []);

  // Функция для инкремента часов
  const incrementHH = useCallback(() => {
    setTime((prev) => {
      const [hh, mm] = unMask(prev);

      const resultHH = String(Number(hh) + 1).padStart(2, "0");

      return `${resultHH}:${mm}`;
    });
  }, []);

  // Функция для декремента часов
  const decrementHH = useCallback(() => {
    setTime((prev) => {
      const [hh, mm] = unMask(prev);

      const resultHH = String(Math.max(Number(hh) - 1, 0)).padStart(2, "0");

      return `${resultHH}:${mm}`;
    });
  }, []);

  // Вызываем handleTimeChange, для исправления минут,
  // в случае если больше 60

  useEffect(() => {
    if (initTime) {
      handleTimeChange(initTime);
    }
  }, []);

  return [time, handleTimeChange, incrementHH, decrementHH];
};

export function FieldTime(props: ITimeField) {
  const [time, handleTimeChange, incrementHH, decrementHH] = useTimeInput(
    props.value
  );

  const marginTop = useMemo(() => {
    return props.class?.split(" ").find((item) => {
      return item.includes("mt");
    });
  }, [props.class]);

  const otherClasses = useMemo(() => {
    return props.class
      ?.split(" ")
      .filter((item) => {
        return !item.includes("mt");
      })
      .join(" ");
  }, [props.class]);

  const handleFocus = () => {
    if (!props.onChange) return;
    const [hh, mm] = unMask(time);
    props.onChange(`${hh}:${mm}`);
  };

  return (
    <View
      style={[
        tw`flex-row items-center relative ${marginTop || ""}`,
        { width: "fit-content", maxWidth: "100%" },
      ]}
    >
      {props.useButtons && (
        <Pressable onPress={decrementHH}>
          <FontAwesomeIcon icon={faArrowLeft} />
        </Pressable>
      )}

      <TextInputMask
        type={"datetime"}
        options={{
          format: "HH:mm",
        }}
        placeholder="--:--"
        value={time}
        maxLength={5}
        onChangeText={handleTimeChange}
        style={[tw`${otherClasses || ""}`, { maxWidth: "100%" }]}
        onBlur={handleFocus}
      />

      {props.useButtons && (
        <Pressable onPress={incrementHH}>
          <FontAwesomeIcon icon={faArrowRight} />
        </Pressable>
      )}
    </View>
  );
}
