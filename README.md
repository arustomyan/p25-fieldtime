# FieldTimeComponent

## Описание

Это тестовое задание задачей которого было разработать FieldTime - компонент ввода времени

## Установка и запуск

1. Клонируйте репозиторий:

```
git clone https://gitlab.com/arustomyan/p25-fieldtime.git
```

2. Установите необходимые зависимости

```
yarn install
```

3. После установки всех зависимостей выполните:

```
npx expo start --web
```

# документация

https://reactnative.dev/

https://docs.expo.dev/

https://tailwindcss.ru/

https://fontawesome.com/docs/web/use-with/react-native

## Автор

### Арустомян Вячеслав:

Telegram - <https://t.me/arustomyan>  
Почта - <v.arustomyan@icloud.com>
