import { Col1 } from "./src/RNCore/components/markup/markup";
import { FieldTime } from "./src/RNCore/components/fields/FieldTime";
import React from "react";
import { EDate } from "./src/RNCore/sugar/date";
import { Text } from "react-native";

export default function App() {
  return (
    <Col1 class={"m-5"}>
      <Text>
        Привет! Ниже должно быть по больше примеров использования готового
        компонента.{" "}
      </Text>
      <Text>Разной ширины, с разным фоном и цветом шрифта</Text>
      <FieldTime class={"mt-2 bg-primary"} value={null} useButtons />
      <FieldTime
        class={"mt-2 bg-danger text-8"}
        value={new EDate().isoTime(false)}
      />
      <FieldTime value={null} class={"w-50 mt-2 bg-success"} />
      <FieldTime value={null} class={"w-50 p-2 mt-2 bg-success"} />
      <FieldTime
        value={new EDate().isoTime(false)}
        class={"w-100 mt-2 bg-primary text-6 p-2"}
        useButtons
      />
      <FieldTime value={"55:99"} class={"w-100 mt-2 bg-primary text-6 p-2"} />
      <FieldTime
        value={new EDate().isoTime(false)}
        class={"w-100 mt-2 bg-primary text-6 p-2"}
      />
      <FieldTime
        value={new EDate().isoTime(false)}
        class={"w-100 mt-2 bg-alphaBlack text-danger text-6 p-2"}
        useButtons
      />
      <FieldTime
        value={new EDate().isoTime(false)}
        class={"w-200 mt-2 bg-levelTwo text-6 p-2"}
      />
      <FieldTime
        value={new EDate().isoTime(false)}
        class={"w-100 mt-2 bg-silver text-10 p-2"}
      />
    </Col1>
  );
}
